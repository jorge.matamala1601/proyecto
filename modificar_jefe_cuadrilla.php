<?php
include("conexion.php");
require 'vista.php';
$rut_jefe_cuadrilla=$_GET['user'];

$consulta = mysqli_query ($con, "SELECT * FROM jefe_cuadrilla where rut_jefe_cuadrilla='$rut_jefe_cuadrilla'");
$modificar=mysqli_fetch_array($consulta)

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
	<title>Modificar</title>
  <link rel="stylesheet" href="css/estilosss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
	
  <div class="encabezado">  
          <h1>Datos del trabajador</h1>
  </div>

<div class="contenedor3">

  <form action="modificar_jefe_cuadrilla2.php" method="POST">
    <div class="form-row">
      <div class="col-md-3">
        <label for="rut_jefe_cuadrilla">Rut del trabajador:</label>
        <input type="text" class="form-control" id="rut_jefe_cuadrilla" value="<?php echo $modificar['rut_jefe_cuadrilla']?>" name="rut_jefe_cuadrilla" readonly>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="nombre">Nombre trabajador:</label>
        <input type="text" class="form-control" id="nombre" value="<?php echo $modificar['nombre']?>" name="nombre" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

      <div class="col-md-4">
        <label for="apellido">Apellidos del trabajador:</label>
        <input type="text" class="form-control" id="apellido" value="<?php echo $modificar['apellido']?>" name="apellido" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-4">
        <label for="telefono">Telefono de contacto:</label>
        <input type="number" class="form-control" id="telefono" value="<?php echo $modificar['telefono']?>" name="telefono" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>      
      <div class="col-md-4">
        <label for="correo">Correo electronico:</label>
        <input type="email" class="form-control" id="correo" value="<?php echo $modificar['correo']?>" name="correo" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-md-6">
        <label for="clave">Clave:</label>
        <input type="pass" class="form-control" id="clave" value="<?php echo $modificar['clave']?>" name="clave" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
      <div class="col-md-4">
        <label for="cuadrilla">Cuadrilla perteneciente:</label>
        <input type="text" class="form-control" id="cod_cuadrilla" value="<?php echo $modificar['cod_cuadrilla']?>" name="cod_cuadrilla" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
    </div>

<div style="margin-top: 5px;">
    <button type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
  </div>
  </div>
</div>
</div>
</div>
</form>

</dir>

</body>
</html>