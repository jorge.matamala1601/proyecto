<?php
   include("conexion.php");

require 'vista.php';


function generarCodigo($longitud) {
 $key = '';
 $pattern = '1234567890';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
   
    <title>Agregar Material</title>
</head>
<body style="background: #CFCBCA;">

      <div class="encabezado">  
          <h1>Lista de material</h1>
  </div>
    
    <div class="contenedor3">
        <form action="agregar_material2.php" method="POST">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <span><b>Codigo</b></span>
                        <input type="text" value="<?php echo generarCodigo(6) ?>" id="idcodigo" name='idcodigo' readonly>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <span><b>Nombre</b></span>
                        <input type="text" class="form-control" id="nombre" name='nombre' required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <span><b> Cantidad</b></span>
                        <input type="number" class="form-control" id="cantidad" name='cantidad' min="1" required>
                    </div>
                </div>
                
                <div class="col-md-12" >
                    <br>
                    <button type="submit" style='width:20%;' class="btn btn-success">Aceptar</button>
                    <a href="inventario.php?cancelado" style='width:20%;' id="cancelar" name="cancelar" class="btn btn-danger">Cancelar</a>
                </div>
            </div>

        </form>
          <?php
             if(!empty($_GET['error'])){
                 if($_GET['error']=='repetido'){
        ?>
            <div class="alert alert-danger" role="alert">
                 El codigo del material esta duplicado.
            </div>
        <?php
                }
            }
         ?>
    </div>
       






   <?php require 'extensiones/scripts.php' ?>
</body>
</html>
