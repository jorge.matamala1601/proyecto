<?php

require 'vista.php';

include("conexion.php");


?>



<!DOCTYPE HTML>
<html lang="en">

<head>
    <title> tipo  de Proyectos</title>
 
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/
  fontello.css">
  <link rel="stylesheet" type="text/css" href="estilosss.css">


</head>
  
  
<body>
  <div class="encabezado">  
          <h1>Lista de tipo de Proyectos </h1>
  </div>


<dir style="margin-left: 18%">
      <button type="button" class="btn btn-success" onclick="window.location='nuevotipoproyecto.php'">nuevo proyecto</button>
</dir>

<div class="contenedor3">  

  <div class="table-responsive">          
    <table class="table table-striped table-hover" id="tabla">
      <thead class="thead-green">
        <tr>
        <th>Codigo</th>
        <th>Nombre </th>
       <th>Descripcion</th>
       <th>editar</th>
        
        
        </tr>
    </thead>
    <tbody class="tbody-green">
        <?php
      
          $consulta7 = mysqli_query ($con, "SELECT * FROM tipodeproyecto");
            while($mostrar4=mysqli_fetch_array($consulta7)){
        ?>
        <tr>
                                    <td><?php echo $mostrar4['cod_tipo'] ?></td>
                                    <td><?php echo $mostrar4['nombre'] ?></td>
                                    <td><?php echo $mostrar4['descripcion'] ?></td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="ver_tipo_promod.php?cod_tipo=<?php echo $mostrar4['cod_tipo'] ?>" class="btn btn-block btn-info">Modificar</a>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" onclick="confirmar(<?php echo $mostrar4['cod_tipo'] ?>)" class="btn btn-block btn-danger">Eliminar</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
        <?php }?>
    </tbody>
    </table>

  </div>


  </div>

</div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });

    </script>

 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script>
    function confirmar(cod_tipo){
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
        title: '¿Está seguro de eliminar?',
        text: "No podrá recuperar la infromacion!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, eliminar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
        }).then((result) => {
        if (result.value) {
            eliminar(cod_tipo)
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelado',
            'el tipo  no se eliminará',
            'error'
            )
        }
        })
    }

	function eliminar(cod_tipo) {
		cadena = "cod_tipo="+cod_tipo;
        //console.log(cadena)

		$.ajax({
			type: "POST",
			url: "eliminartipo.php",
			data: cadena,
			success: function(r) {
				if (r == 1) {
					Swal.fire({
						icon: 'success',
						title: 'Eliminación exitosa',
						text: "Eliminado",

						showCancelButton: false,
						confirmButtonColor: '#0867F4',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Continuar'
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					})

				} else {
					Swal.fire({
						icon: 'error',
						title: 'No se ha podido eliminar',
						text: 'Ocurrió un error interno'
					})
				}
			}
		})
	}
</script>
</body>
</html>